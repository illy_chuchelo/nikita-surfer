<?php

namespace App\Http\Controllers\Admin;

use App\Music;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MusicsController extends Controller
{
    public function index()
    {
        $musics = Music::all();
        return view('admin.musics.index', ['musics'=>$musics]);
    }

    public function create()
    {
        return view('admin.musics.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [

            'link' =>  'required',
            'title' =>  'required'

        ]);

        $music = Music::add($request->all());
        $music->toggleStatus($request->get('in_home'));
        $music->toggleSingle($request->get('is_single'));

        return redirect()->route('musics.index');
    }

    public function edit($id)
    {
        $music = Music::find($id);

        return view('admin.musics.edit', compact(
            'music'
        ));

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'link' =>  'required',
            'title' =>  'required'
        ]);

        $music = Music::find($id);
        $music->edit($request->all());
        $music->toggleStatus($request->get('in_home'));
        $music->toggleSingle($request->get('is_single'));

        return redirect()->route('musics.index');
    }

    public function destroy($id)
    {
        Music::find($id)->remove();
        return redirect()->route('musics.index');
    }
}
