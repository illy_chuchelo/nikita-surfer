<?php

namespace App\Http\Controllers\Admin;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    public function index()
    {
        $videos = Video::all();
        return view('admin.videos.index', ['videos'=>$videos]);
    }

    public function create()
    {

        return view('admin.videos.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' =>'required',
            'description' => 'required',
            'link' =>  'required'
        ]);

        $video = Video::add($request->all());

        return redirect()->route('videos.index');
    }

    public function edit($id)
    {
        $video = Video::find($id);

        return view('admin.videos.edit', compact(
            'video'
        ));

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' =>'required',
            'description' => 'required',
            'link' =>  'required'
        ]);

        $video = Video::find($id);
        $video->edit($request->all());

        return redirect()->route('videos.index');
    }

    public function destroy($id)
    {
        Video::find($id)->remove();
        return redirect()->route('videos.index');
    }
}
