<?php

namespace App\Http\Controllers;

use App\Music;
use App\Video;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $music = Music::getHomeMusic()->first();

        return view('pages.index', compact('music'));
    }

    public function music()
    {
        $firstPost  = Music::getMusic()->first();
        $secondPost = Music::getMusic()->where('id', '<', $firstPost->id)->first();
        $musics     = Music::getMusic()->WhereNotIn('id', [$firstPost->id,$secondPost->id])->get();

        return view('pages.music', compact('firstPost', 'secondPost', 'musics'));
    }

    public function video()
    {
        $videos = Video::all();

        return view('pages.video', compact('videos'));
    }

    public function contact()
    {
        return view('pages.contact');
    }

}
