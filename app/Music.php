<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = ['link', 'title'];//поля для заполнения

    public static function add($fields)//добавление поста
    {
        $music = new static;
        $music->fill($fields);          //заполняет поля из $fillable
        $music->save();

        return $music;
    }

    public function edit($fields)      //изменить пост
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()           //удалить пост
    {
        $this->delete();

    }

    public function unsetHome()
    {
        $this->in_home = 0;
        $this->save();
    }

    public function setHome()
    {
        $this->in_home = 1;
        $this->save();
    }

    public function unsetSingle()
    {
        $this->is_single = 0;
        $this->save();
    }

    public function setSingle()
    {
        $this->is_single = 1;
        $this->save();
    }

    public function toggleStatus($value)
    {
        if($value == null)
        {
            return $this->unsetHome();
        }

        return $this->setHome();
    }

    public function toggleSingle($value)
    {
        if($value == null)
        {
            return $this->unsetSingle();
        }

        return $this->setSingle();
    }

    public static function getHomeMusic()
    {
        return self::where('in_home', 1)->orderBy('id', 'desc');
    }

    public static function getMusic()
    {
        return self::orderBy('id', 'desc');
    }
}
