<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'description', 'link'];//поля для заполнения

    public static function add($fields)//добавление поста
    {
        $video = new static;
        $video->fill($fields);          //заполняет поля из $fillable
        $video->save();

        return $video;
    }

    public function edit($fields)      //изменить пост
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()           //удалить пост
    {
        $this->delete();

    }


}
