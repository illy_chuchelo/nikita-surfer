<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="treeview">
        <a href="{{route('admin')}}">
            <i class="fa fa-dashboard"></i> <span>Админ-панель</span>
        </a>
    </li>
    <li><a href="{{route('musics.index')}}"><i class="fa fa-soundcloud"></i> <span>Музыка</span></a></li>
    <li><a href="{{route('videos.index')}}"><i class="fa fa-video-camera"></i> <span>Видео</span></a></li>


</ul>