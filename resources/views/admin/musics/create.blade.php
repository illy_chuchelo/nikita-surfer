@extends('admin.layout')

@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
        {!! Form::open(['route' => 'musics.store' ]) !!}

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем Музыку</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ссылка с SoundCloud</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="link">
                        </div>


                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('in_home', '1')}}
                            </label>
                            <label>
                                На главную
                            </label>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

         {!! Form::close() !!}
        </section>
        <!-- /.content -->
    </div>
@endsection