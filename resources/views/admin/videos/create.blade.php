@extends('admin.layout')

@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
        {!! Form::open(['route' => 'videos.store' ]) !!}

        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем Видео</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ссылка на YouTube</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="link">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Описание</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control" ></textarea>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

            {!! Form::close() !!}
        </section>
        <!-- /.content -->
    </div>
@endsection