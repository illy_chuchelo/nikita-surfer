@extends('layout')

@section('content')
<body id="background-color">

<div class="container-fluid">
    <div class="row align-items-center justify-content-center" style="background-color: #ff4d4d; height: 10%">
        <h1 style="letter-spacing: 10px; "><strong>CONTACTS</strong></h1>
    </div>
</div>

<div class="container-fluid" style="padding-top: 5%">
    <div class="row ">
        <div class="col-md-12 " >
            <div class="well well-sm">
                        <legend class="text-center" style="color: white">Contact us</legend>
                        @if(session('status'))
                            <div class="alert alert-danger">
                                {{session('status')}}
                            </div>
                        @endif
                        {!! Form::open(['url' => '/send', 'class' => 'contact-form']) !!}
                        <div class="row text-white " style="padding-top: 5%; padding-right: 15%; padding-left: 15%">
                            <div class="col-md-6 form-group" >
                                <label  for="name">Name</label>
                                <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
                            </div>
                            <div class="col-md-6 form-group" >
                                <label for="email">Your E-mail</label>
                                <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
                            </div>
                            <div class="col-md-12 form-group" >
                                <label for="message">Your message</label>
                                <textarea class="form-control" id="message" name="text" placeholder="Please enter your message here..." rows="8"></textarea>
                            </div>
                        </div>
                        <div class="text-center form-group">
                            <button type="submit" class="btn btn-danger btn-lg">Send</button>
                        </div>
                        {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@include('_footer')
@endsection
