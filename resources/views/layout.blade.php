<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/front.css" >

    <title>Nikita Surfer</title>


</head>

<header>
    <nav class=" navbar sticky-top navbar-expand-lg navbar-dark " style="background-color: #21181D;">

        <a class="navbar-brand" style="padding-right: 30%" href="#">Nikita Surfer</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <div class="navbar-nav  ">
                <!-- Добавить цвета и сделать ровнее -->
                    <a class="nav-item nav-link" href="/">Home</a>
                    <a class="nav-item nav-link" href="/music">Music</a>
                    <a class="nav-item nav-link" href="/video">Video</a>
                    <a class="nav-item nav-link" href="/contact">Contacts</a>
            </div>

        </div>

    </nav>

</header>

<body>

@yield('content')

<!-- Link -->
<div class="ssk-sticky ssk-center ssk-lg ssk-right">
    <a href="" class="ssk ssk-instagram" style="background-color: #e4b9b9" data-ssk-ready="true"></a>
    <a href="" class="ssk ssk-vk" data-ssk-ready="true"></a>
    <a href="" class="ssk ssk-youtube" style="background-color: red" data-ssk-ready="true"></a>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>