@extends('layout')


@section('content')
<body id="home">

    <div class="container-fluid" >

        <div class="row no-gutters">
            <div class="col-lg-6 col-md-6 col-md-6 order-3">
                <div class="row no-gutters">
                    <div class="col-lg-3 "></div>
                    <div class="col-lg-8 " style="padding-top: 10%;">
                        <iframe width="100%" height="600" scrolling="no" frameborder="no" allow="autoplay" src="{{$music->link}}"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 order-2 order-md-12">

            </div>
        </div>
    </div>

@endsection
