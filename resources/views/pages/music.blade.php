@extends('layout')

@section('content')
<body id="background-color">

<div class="container-fluid">
    <div class="row align-items-center justify-content-center" style="background-color: #ff4d4d; height: 10%">
         <h1 style="letter-spacing: 10px; "><strong>MUSIC</strong></h1>
    </div>
</div>

<div class="container-fluid">

    <div class="row ">

            <div class="col-lg-6 col-sm-6 align-self-center" style="padding-top: 5%; padding-left: 7%; padding-right: 7%; ">
                <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="{{$firstPost->link}}"></iframe>
            </div>
            <div class="col-lg-6 col-sm-6 align-self-center" style="padding-top: 5%; padding-left: 7%; padding-right: 7%; ">
                <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="{{$secondPost->link}}"></iframe>
            </div>

        @foreach($musics as $music)
            <div class="col-lg-6 col-sm-6 align-self-center" style="padding-top: 5%; padding-left: 10%; padding-right: 10%;">
                <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="{{$music->link}}"></iframe>
            </div>
        @endforeach

    </div>
</div>
@include('_footer')
@endsection

