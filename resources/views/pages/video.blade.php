@extends('layout')

@section('content')
    <body id="background-color">

    <div class="container-fluid">
        <div class="row align-items-center justify-content-center" style="background-color: #ff4d4d; height: 10%">
            <h1 style="letter-spacing: 10px; "><strong>VIDEO</strong></h1>
        </div>
    </div>

    <div class="container-fluid">
        @php  $i=0; @endphp
        @foreach($videos as $video)
            @php  $i++; @endphp
            @if($i % 2 != 0)
                <div class="row align-items-center " style="background-color: #ffffff; padding-top: 5%; padding-bottom: 5% ">

                    <div class="col-lg-6 " style="padding-left: 5%; padding-right: 5%; ">
                        <iframe width="100%" height="315" src="{{$video->link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    <div class="col-lg-6 " style="padding-left: 5%; padding-right: 5%;">
                        <h6 class="text-uppercase" style="color: black; font-size: 30px"><strong>{{$video->title}}</strong></h6>

                        <span style="color: black; font-size: 25px">{!! $video->description !!}</span>

                    </div>

                </div>
            @else
                <div class="row align-items-center" style=" padding-top: 5%; padding-bottom: 5% ">

                    <div class="col-lg-6  text-right text-white " style="padding-left: 5%; padding-right: 5%;">
                        <h6 class="text-uppercase" style="color: white; font-size: 30px"><strong>{{$video->title}}</strong></h6>
                        <span style="font-size: 25px">{!! $video->description !!}</span>
                    </div>

                    <div class="col-lg-6 " style="padding-left: 5%; padding-right: 5%; ">
                        <iframe width="100%" height="315" src="{{$video->link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                </div>
            @endif
        @endforeach

    </div>
    @include('_footer')
@endsection