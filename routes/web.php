<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/music', 'HomeController@music')->name('music');
Route::get('/video', 'HomeController@video')->name('video');
Route::get('/contact', 'ContactController@contact');
Route::post('/send', 'ContactController@send');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function (){

    Route::get('/', 'DashboardController@index')->name('admin');
    Route::resource('/musics', 'MusicsController');
    Route::resource('/videos', 'VideosController');
});

Route::group(['middleware' => 'guest'], function (){

    Route::get('/login', 'AuthController@loginForm')->name('login');
    Route::post('/login', 'AuthController@login');

});

Route::group(['middleware' => 'auth'], function (){

    Route::get('/logout', 'AuthController@logout');

});

